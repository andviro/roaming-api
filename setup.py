#!/usr/bin/env python
# -*- coding: utf-8 -*-
import ez_setup
ez_setup.use_setuptools()

from setuptools import setup

depends = [
    "xml_orm", "edo_schemas", "cryptoapy", "pies", "nose", "m2crypto",
    "lxml", "requests", "ndg-httpsclient"
]

setup_params = dict(
    name='roaming',
    version='0.0.9',
    packages=['roaming', 'roaming.state'],
    author='Andrew Rodionoff',
    author_email='andviro@gmail.com',
    license='MIT',
    description='Transport container utility classes',
    setup_requires=['nose', 'coverage'],
    install_requires=depends,
    dependency_links=["https://bitbucket.org/andviro/schemas/get/master.zip#egg=edo_schemas"],
    extras_require={
            'sqlalchemy': ['sqlalchemy']
    },
)

setup(**setup_params)
