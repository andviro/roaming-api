#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import absolute_import, division, unicode_literals, print_function
from fabric.api import local
import os
import platform

if platform.system() == 'Windows':
    scriptsdir = ".env\\Scripts\\"
else:
    scriptsdir = ".env/bin/"

python = os.path.join(scriptsdir, 'python')
nose = os.path.join(scriptsdir, 'nosetests')


def develop():
    if not os.path.isdir('.env'):
        local("virtualenv .env")
    local("{0} setup.py develop".format(python))


def test():
    local(nose + ' -v')
