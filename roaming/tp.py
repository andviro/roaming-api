#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import absolute_import, division, unicode_literals, print_function
from pies.overrides import *  # noqa
from zipfile import ZipFile
from io import BytesIO
from datetime import datetime
import os
import re
import mimetypes
from uuid import uuid4
from requests import post
from schemas.edi import ContainerEDI
from .schemas import Receipts, Message, Receipt, EDIGate
from .rsa import extract_container, sign_container
from .groups import Groups

gateway = EDIGate()

CMS_NAME_RE = re.compile(r'^[a-z0-9_]{1,100}$', re.I)
MSGID_NAME_RE = re.compile(r'^[a-z0-9]{32}$', re.I)

DIRECTION_IN = 0
DIRECTION_OUT = 1


def maybe_unpack(data):
    try:
        zf = ZipFile(BytesIO(data))
        return zf.read(zf.namelist()[0])
    except Exception:
        return data


def _guess_type(fn):
    content_type, _ = mimetypes.guess_type('file:///{0}'.format(fn))
    return content_type or 'application/octet-stream'


class DummyState(object):
    pass

dummy_state = DummyState()


def _autoload(data, fn=None):
    '''
    Автоматическая загрузка бинарника из файла, файлового объекта или строки.
    Служебная функция, вызывается из конструкторов классов. Возвращает пару из
    бинарника и имени файла, если удалось его определить по входным параметрам.

    '''
    if isinstance(data, str):
        return open(data, 'rb').read(), data
    elif isinstance(data, bytes):
        return data, fn
    elif hasattr(data, 'read'):
        return data.read(), getattr(data, 'name', None) or fn
    else:
        raise TypeError("Input data of type '{0}' not supported"
                        .format(data.__class__.__name__))


class TransportPackage(object):

    """
    Инкапсуляция базовой функциональности ТП

    """

    state = dummy_state

    def __init__(self, uid=None):
        '''
        Создание нового, пустого ТП

        '''
        self.error = None
        self._zip_data = None
        self._zip_file = None
        self._messages = []
        self._receipts = []
        self.uid = uid or uuid4().hex

    def add(self, obj):
        '''
        Добавление объект `obj` в ТП. Объект должен быть либо экземпляром класса
        `LogicalMessage`, либо квитанцией. Нельзя добавлять в один и тот же ТП
        и квитанции и ЛС.

        '''
        if isinstance(obj, Receipt):
            self._receipts.append(obj)
        elif isinstance(obj, LogicalMessage):
            self._messages.append(obj)
        else:
            raise ValueError('Can not add object of type {0} to package'
                             .format(obj.__class__.__name__))
        if len(self._receipts) and len(self._messages):
            raise ValueError('Transport package should not contain both messages and receipts')

    def save_zip(self, fn=None):
        '''
        Упаковка содержимого ТП в ZIP.

        * `fn` - имя файла, необязательный параметр. Если задано, то сохранение
        производится в файл на диске, на который указывает имя. В противном
        случае функция возвращает архив в виде байтовой строки.

        '''
        if self._zip_data:
            return self._zip_data

        buf = BytesIO()
        zf = ZipFile(buf, 'w')
        if self._receipts:
            container = Receipts()
            for rec in self._receipts:
                if rec.receipt_type == 'error':
                    container.error.append(rec)
                elif rec.receipt_type == 'unknown_id':
                    container.unknown_id.append(rec)
                elif rec.receipt_type == 'success':
                    container.success.append(rec)
            zf.writestr('receipts.xml', bytes(container))
        else:
            for msg in self._messages:
                for msg_file in msg.namelist():
                    zf.writestr('{0}/{1}'.format(msg.uid, msg_file),
                                msg.read(msg_file))
        zf.close()
        data = buf.getvalue()
        if not fn:
            return data
        open(fn, 'wb').write(data)

    def save_cms(self, key, cert, fn=None):
        '''
        Сохранение содержимого ТП в подписанный CMS.

        * `key` - путь к файлу закрытого ключа в PEM
        * `cert` - путь к файлу сертификата в PEM
        * `fn` - имя файла, необязательный параметр. Если задано, то сохранение
        производится в файл на диске, на который указывает имя. В противном
        случае функция возвращает подписанный CMS в виде байтовой строки.

        '''
        data = self.save_zip()
        cms = sign_container(data, key, cert)
        if not fn:
            return cms
        open(fn, 'wb').write(cms)

    @classmethod
    def load(cls, data, name=None):
        """Загрузка ТП из файла или бинарных данных

        * `data` - файлоподобный объект, (должен иметь `read()`) или имя файла

        * `name` - имя файла, необязательно, если файлоподобный объект на входе
               имеет поле `name`, или если `data` - само по себе имя файла

        Возвращает новый экземпляр. Если загрузка не прошла, в экземпляре поле
               `error` приобретает непустое значение.

        """
        result = cls()
        result.error = None

        result._data, result._fn = _autoload(data, name)
        if result._fn is None:
            raise ValueError("File name for CMS must be supplied")

        result.uid = os.path.splitext(os.path.basename(result._fn))[0]
        if not CMS_NAME_RE.match(result.uid):
            result.error = 'Invalid CMS name format'

        try:
            result._zip_data, result.sender_thumb = extract_container(result._data)
        except Exception as exc:
            result._msg = result.sender_thumb = result._zip_data = None
            result.error = 'Error decoding CMS data: {0!s}'.format(exc)
            return result

        try:
            result._zip_file = ZipFile(BytesIO(result._zip_data))
        except Exception as exc:
            result._zip_file = None
            result.error = 'Error loading zip data: {0!s}'.format(exc)
            return result

        result.error = result.check_structure()
        return result

    def check_structure(self):
        '''
        Проверка структуры файлов в ТП. Не предназначена для непосредственного
        вызова, используется при загрузке из файла.

        '''
        names = self._zip_file.namelist()
        if names == ['receipts.xml']:
            return
        for name in names:
            pathlist = name.split('/')
            if len(pathlist) != 2:
                return u'Invalid path in archive: {0}'.format(name)
            if not MSGID_NAME_RE.match(pathlist[0]):
                return u'Invalid subdirectory name: {0}'.format(pathlist[0])

    def unpack(self):
        '''
        Извлекает из ТП квитанции или логические сообщения. Создает генератор,
        который возвращает либо экземпляры класса `Receipt` либо экземпляры
        `LogicalMessage`.

        XXX: В хранилище состояния извлеченные сообщения нужно добавлять отдельным
        вызовом:

        tp = TransportPackage.load(data)
        for msg in tp.unpack():
            ...
            tp.state.add_message(msg.uid)

        '''
        assert not self.error, "Package with errors can't be unpacked"
        assert self._zip_file, "Package is not associated with zip file"
        names = self._zip_file.namelist()
        if names == ['receipts.xml']:
            receipts = Receipts.load(self._zip_file.open('receipts.xml'))
            for error in receipts.error:
                yield error
            for unk_id in receipts.unknown_id:
                yield unk_id
            for succ in receipts.success:
                yield succ
        else:
            for name in set(n.split('/')[0] for n in names):
                yield LogicalMessage.from_package(self, name)

    def __unicode__(self):
        return 'TransportPackage({0})'.format(self.uid)

    @staticmethod
    def send(url, receipt_url, key, cert, body, uid):
        """ Отправка CMS-сообщения по протоколу HTTPS

        * `url` - URL вида 'https://тырпыр.рф/куда/'
        * `receipt_url` - URL для заголовка Send-Receipt-To
        * `key` - путь к файлу закрытого ключа в PEM
        * `cert` - путь к файлу сертификата в PEM
        * `body` - содержимое файла
        * `uid` - имя файла без расширения

        """
        headers = {
            'Send-Receipt-To': receipt_url,
            'Content-Disposition': 'attachment; filename="{0}.cms"'.format(uid),
        }

        try:
            response = post(url, headers=headers, cert=(cert, key),
                            verify=False, data=body)
            status = response.status_code
            reason = response.reason
            data = response.content
        except Exception as exc:
            status = 0
            reason = ('Error sending package to {0}: {1}'
                      .format(url, type(exc).__name__))
            data = str(exc)
        return status, reason, data


class LogicalMessage(object):

    """
    Инкапсуляция логического сообщения роуминга

    """

    state = dummy_state

    def __init__(self, uid=None):
        '''
        Создание пустого ЛС, неприсоединенного к транспортному пакету.

        '''
        self.uid = uid or uuid4().hex
        self._contents = {}
        self.package = None
        self.error = None
        self.descriptor = None
        self.unknown_ids = []

    def __unicode__(self):
        return 'LogicalMessage({0})'.format(self.uid)

    @classmethod
    def from_package(cls, package, uid):
        '''
        Создание экземпляра ЛС как дочернего объекта ТП.

        * `package` - объект `TransportPackage`
        * `uid` - идентификатор сообщения в пакете

        Результат функции - новый экземпляр, возможно с установленным полем `error`

        '''
        res = cls(uid=uid)
        res.package = package
        res.state = package.state
        desc_fn = '{0}/description.xml'.format(res.uid)
        if desc_fn not in res.package._zip_file.namelist():
            res.error = 'description.xml not found'
            return res

        try:
            res.descriptor = Message.load(res.package._zip_file.open(desc_fn))
        except Exception as exc:
            res.error = str(exc)
            print(res.error)
            return

        return res

    def namelist(self):
        '''
        Список имен файлов в ЛС, по аналогии с объектом ZipFile

        Возвращает генератор имен.
        '''
        if self.package:
            return sorted(fn.split('/')[1] for fn in self.package._zip_file.namelist()
                          if fn.startswith(self.uid) and not fn.endswith('/'))
        return sorted(self._contents.keys())

    def read(self, fn):
        '''
        Загрузка содержимого файла из ЛС

        * `fn` - имя файла
        Результат - байтовая строка

        '''

        if self.package:
            return self.package._zip_file.read('{0}/{1}'.format(self.uid, fn))
        return self._contents[fn]

    @classmethod
    def from_edi(cls, edi):
        """Трансформация файла-контейнера ТрансИнф в объект ЛС

        * `edi` - Объект класса ContainerEDI

        Возвращает новый объект класса LogicalMessage

        """
        res = cls()

        # серия объединяет в себя несколько ЛС
        series = res.state.get_sequence(edi.uid)
        # контейнер из БД, ассоциированный с данным EDI
        state_pkg = res.state.get_package(edi.package)

        # обеспечим, что контейнер в БД имеет верный код транзакции
        res.state.update_package(state_pkg, edi.trans_code)

        created = series is None
        if created:  # серия не найдена, начало новой серии
            if edi.trans_code != '01':
                # проверка, может ли пакет EDI начинать серию,
                # т.е. подходящий ли у него номер транзакции.
                res.error = 'Transaction code {0} can not start new series'.format(edi.trans_code)
                return res
            series = res.state.add_sequence(edi.doc_code, uid=edi.uid)

        res.descriptor = desc = Message(
            sender=edi.sender.uid,
            receiver=edi.receiver.uid,
            send_date=datetime.now().strftime('%Y-%m-%dT%H:%M:%SZ'),
        )

        state_msg = res.state.add_message(res.uid)
        res.state.add_package(series, state_msg, state_pkg)

        main_doc = edi.main_document
        for doc in edi.docs:
            # тип документа РОСЭУ должен однозначно определяться по
            # документообороту/транзакции/типу документа EDI.
            try:
                roseu_doctype = gateway[edi.doc_code][edi.trans_code][doc.type_code]
            except KeyError:
                res.error = ('Error converting doctype {0} for transaction {1}'
                             .format(doc.type, edi.transaction))
                return res

            d = desc.Document(uid=doc.uid,
                              filename=doc.orig_filename,
                              doc_type=roseu_doctype,
                              date=datetime.now())

            res.state.add_document(state_pkg,
                                   doc.uid,
                                   doc.type_code)

            if doc.uid != main_doc.uid:
                # Неглавные документы в пакете EDI привязаны к главному и
                # находятся вне серии
                d.related_doc = main_doc.uid
            elif not created:
                # Мы имеем дело с главным документом, и он либо открывает
                # серию, либо привязан к документу серии.
                # XXX: Привязываем к первому документу серии, с наименьшим
                # номером транзакции.

                d.related_doc = res.state.get_main_doc(series)

            res._contents['{0}.bin'.format(doc.uid)] = maybe_unpack(edi.read(doc.content.filename))

            desc.document.append(d)
            for sig in doc.signatures:
                sig_uid = os.path.splitext(sig.filename)[0]
                res._contents['{0}.p7s'.format(sig_uid)] = edi.read(sig.filename)
                desc.signature.append(
                    desc.Signature(signer=edi.sender.uid,
                                   uid=sig_uid,
                                   related_doc=doc.uid))
        res._contents['description.xml'] = bytes(desc)
        return res

    def convert_to_edi(self):
        """Трансформация ЛС в объект класса ContainerEDI.

        """
        # ЛС может содержать несколько документов, ссылающихся друг на друга
        # через поле КДокументу.
        # Группа документов состоит из главного и дополнительных, ссылающихся
        # на него. Главный может ссылаться на документ из уже существующей
        # серии, либо начинать новую серию.
        res = []

        docs = {}
        groups = Groups()

        state_msg = self.state.get_message(self.uid)
        if not state_msg:
            self.error = 'Message {0} not found in database'.format(self.uid)
            return []

        for doc in self.descriptor.document:
            docs[doc.uid] = doc  # Для быстрого доступа к документу по UID
            groups.add(doc.uid, doc.related_doc[0] if len(doc.related_doc) else None)

        for (ref, grp) in groups:
            created = ref is None
            main_doc = docs[grp[0]]  # Главный документ группы - первый
            if created:
                # Документ начинает новую серию транзакций
                edi_doc_code = gateway[main_doc.doc_type]
                if not edi_doc_code:
                    self.error = 'Unsupported ROSEU doctype: {0}'.format(main_doc.doc_type)
                    return []
                series = self.state.add_sequence(edi_doc_code)
            else:
                # Получаем серию по связанному документу
                state_doc = self.state.get_document(ref)
                if not state_doc:
                    self.unknown_ids.append(ref)
                    continue
                series = self.state.lookup_sequence(state_doc)
                edi_doc_code = series.edi_code

            series_transactions = list(sorted(pkg.trans_code for pkg in series.packages))
            last_transaction = series_transactions[-1] if len(series_transactions) else 0
            edi_trans_code = gateway.get_transaction_code(edi_doc_code, last_transaction, main_doc.doc_type)

            if not edi_trans_code:
                self.error = 'Transaction not found for ROSEU doctype {0}'.format(main_doc.doc_type)
                return []

            edi = ContainerEDI(
                doc_code=edi_doc_code,
                trans_code=edi_trans_code,
                soft_version='Roaming-API 0.3',
                uid=series.uid,
                sender=ContainerEDI.Sender(self.descriptor.sender),
                receiver=ContainerEDI.Receiver(self.descriptor.receiver),
                sos=ContainerEDI.Sos(self.descriptor.sender[:3]),
            )  # Каждая группа помещается в один контейнер EDI

            delayed_docs = []
            for docid in grp:
                doc = docs[docid]
                edi_type_code = gateway[edi_doc_code][edi_trans_code].get(doc.doc_type)
                if not edi_type_code:
                    self.error = 'EDI doctype not found for ROSEU doctype {0}'.format(
                        main_doc.doc_type)
                    return []

                edi.add_file(
                    filename=doc.filename,
                    doc_type=edi_type_code,
                    content_type=_guess_type(doc.filename),  # XXX: магия магия
                    content=self.read(doc.uid + '.bin'),
                    signatures=[self.read(sig.uid + '.p7s')
                                for sig in self.descriptor.signature
                                if sig.related_doc == doc.uid],
                    sig_role=u'абонент')
                delayed_docs.append((docid, edi_type_code))

            # res.error = None
            res.append(edi)
            pkg_fn = getattr(edi._meta, 'package', '').format(self=edi)
            state_pkg = self.state.get_package(pkg_fn)
            self.state.add_package(series, state_msg, state_pkg)
            for docid, doc_code in delayed_docs:
                self.state.add_document(state_pkg, docid, doc_code)
        return res
