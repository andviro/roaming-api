# coding: utf-8
from __future__ import unicode_literals, print_function
from xml_orm.core import Schema
from xml_orm.fields import SimpleField, ComplexField, DecimalField,\
    BooleanField, CharField, DateTimeField


class Message(Schema):
    document = ComplexField(
        ref='Document',
        tag='Документ',
        minOccurs=0,
        maxOccurs='unbounded',
        qualify=True,
    )
    signature = ComplexField(
        ref='Signature',
        tag='ЭП',
        minOccurs=0,
        maxOccurs='unbounded',
        qualify=True,
    )
    sender = CharField.A(
        tag='Отправитель',
        minOccurs=1,
        maxOccurs=1,
        qualify=False,
        max_length=46,
        min_length=4,
    )
    receiver = CharField.A(
        tag='Получатель',
        minOccurs=1,
        maxOccurs=1,
        qualify=False,
        max_length=46,
        min_length=4,
    )
    send_date = SimpleField.A(
        tag='ДатаОтправки',
        minOccurs=1,
        maxOccurs=1,
        qualify=False,
        pattern='.+T.+Z',
    )

    class Meta:
        root = 'Сообщение'
        namespace = 'http://www.roseu.org/images/stories/roaming/logical-message-v1.xsd'
        encoding = 'utf-8'


class Document(Schema):
    related_doc = SimpleField(
        tag='КДокументу',
        minOccurs=0,
        maxOccurs='unbounded',
        qualify=True,
        pattern='[a-z0-9]{32}',
    )
    internal_id = SimpleField(
        tag='ИдВнутренний',
        minOccurs=0,
        maxOccurs=1,
        qualify=True,
    )
    deal_id = SimpleField(
        tag='ИдСделки',
        minOccurs=0,
        maxOccurs=1,
        qualify=True,
    )
    number = SimpleField(
        tag='Номер',
        minOccurs=0,
        maxOccurs=1,
        qualify=True,
    )
    date = DateTimeField(
        tag='Дата',
        minOccurs=1,
        maxOccurs=1,
        qualify=True,
        format='%Y-%m-%d',
    )
    sum = DecimalField(
        tag='Сумма',
        minOccurs=0,
        maxOccurs=1,
        qualify=True,
        max_digits=18,
    )
    sum_nds = DecimalField(
        tag='СуммаНДС',
        minOccurs=0,
        maxOccurs=1,
        qualify=True,
        max_digits=18,
    )
    uid = SimpleField.A(
        tag='ИдДокумента',
        minOccurs=1,
        maxOccurs=1,
        qualify=False,
        pattern='[a-z0-9]{32}',
    )
    doc_type = SimpleField.A(
        tag='ТипДокумента',
        minOccurs=1,
        maxOccurs=1,
        qualify=False,
        restrict=[
            'Неформализованный',
            'ОтказПодписи',
            'ТН',
            'ТТН',
            'Акт',
            'АктСверки',
            'ПлатПоруч',
            'Договор',
            'Заказ',
            'СФ',
            'ИзвещениеОПолучении',
            'УведомлениеОбУточнении',
            'ПредложениеОбАннулировании',
            'Торг12ТитулПродавца',
            'Торг12ТитулПокупателя',
            'АктТитулЗаказчика',
            'АктТитулИсполнителя',
            'СтруктурированныеДанные',
            'Регистрация',
            'ОтветНаРегистрацию',
            'Приглашение',
            'ОтветНаПриглашение',
            'ПодтверждениеДатыОтправки',
            'ПодтверждениеДатыПоступления'
        ],
    )
    signature_required = BooleanField.A(
        tag='ОжидаетсяПодписьПолучателя',
        minOccurs=0,
        maxOccurs=1,
        qualify=False,
        default='false',
    )
    filename = SimpleField.A(
        tag='ИмяФайла',
        minOccurs=1,
        maxOccurs=1,
        qualify=False,
        pattern='[^/\\\\:?*]{1,250}',
    )

    class Meta:
        root = 'Документ'


class Signature(Schema):
    signer = CharField.A(
        tag='Подписант',
        minOccurs=1,
        maxOccurs=1,
        qualify=False,
        max_length=46,
        min_length=4,
    )
    uid = SimpleField.A(
        tag='ИдЭП',
        minOccurs=1,
        maxOccurs=1,
        qualify=False,
        pattern='[a-z0-9]{32}',
    )
    related_doc = SimpleField.A(
        tag='КДокументу',
        minOccurs=1,
        maxOccurs=1,
        qualify=False,
        pattern='[a-z0-9]{32}',
    )

    class Meta:
        root = 'ЭП'


class Receipt(object):
    pass


class Receipts(Schema):
    error = ComplexField(
        ref='Error',
        tag='ОшибкаОбработки',
        minOccurs=0,
        maxOccurs='unbounded',
        qualify=True,
    )
    unknown_id = ComplexField(
        ref='UnknownID',
        tag='НеизвестныйИд',
        minOccurs=0,
        maxOccurs='unbounded',
        qualify=True,
    )
    success = ComplexField(
        ref='Success',
        tag='Успех',
        minOccurs=0,
        maxOccurs='unbounded',
        qualify=True,
    )

    class Meta:
        root = 'Квитанции'
        namespace = 'http://www.roseu.org/images/stories/roaming/logical-message-v1.xsd'


class Error(Schema, Receipt):
    receipt_type = 'error'

    description = SimpleField(
        tag='Описание',
        minOccurs=1,
        maxOccurs=1,
        qualify=True,
    )
    msg_id = SimpleField.A(
        tag='ИдЛС',
        minOccurs=1,
        maxOccurs=1,
        qualify=False,
        pattern='[a-z0-9]{32}',
    )

    class Meta:
        root = 'ОшибкаОбработки'


class UnknownID(Schema, Receipt):
    receipt_type = 'unknown_id'

    uid = SimpleField(
        tag='Ид',
        minOccurs=1,
        maxOccurs='unbounded',
        qualify=True,
        pattern='[a-z0-9]{32}',
        doc='Перечень неизвестных системе идентификаторов',
    )
    description = SimpleField(
        tag='Описание',
        minOccurs=1,
        maxOccurs=1,
        qualify=True,
    )
    msg_id = SimpleField.A(
        tag='ИдЛС',
        minOccurs=1,
        maxOccurs=1,
        qualify=False,
        pattern='[a-z0-9]{32}',
    )

    class Meta:
        root = 'НеизвестныйИд'


class Success(Schema, Receipt):
    receipt_type = 'success'

    msg_id = SimpleField.A(
        tag='ИдЛС',
        minOccurs=1,
        maxOccurs=1,
        qualify=False,
        pattern='[a-z0-9]{32}',
    )

    class Meta:
        root = 'Успех'


_edo_map = [
    {
        "transaction_name": "СчетФактураПродавец",
        "name": "СФ",
        "transaction_code": "01",
        "edo_code": "20",
        "document_code": "01",
        "edo_name": "СчетФактура",
        "document_name": "счетфактура"
    },
    {
        "transaction_name": "ИзвещениеПолученияСФПокупатель",
        "name": "ИзвещениеОПолучении",
        "transaction_code": "06",
        "edo_code": "20",
        "document_code": "06",
        "edo_name": "СчетФактура",
        "document_name": "извещениеОПолучении"
    },
    {
        "transaction_name": "УведомлениеОбУточненииПокупатель",
        "name": "УведомлениеОбУточнении",
        "transaction_code": "07",
        "edo_code": "20",
        "document_code": "07",
        "edo_name": "СчетФактура",
        "document_name": "уведомлениеОбУточнении"
    },
    {
        "transaction_name": "",
        "name": "Неформализованный",
        "transaction_code": "",
        "edo_code": "",
        "document_code": "",
        "edo_name": "",
        "document_name": "неформализованный"
    },
    {
        "transaction_name": "",
        "name": "ОтказПодписи",
        "transaction_code": "",
        "edo_code": "",
        "document_code": "",
        "edo_name": "",
        "document_name": "отказПодписи"
    },
    {
        "transaction_name": "",
        "name": "ТН",
        "transaction_code": "",
        "edo_code": "",
        "document_code": "",
        "edo_name": "",
        "document_name": "тн"
    },
    {
        "transaction_name": "",
        "name": "ТТН",
        "transaction_code": "",
        "edo_code": "",
        "document_code": "",
        "edo_name": "",
        "document_name": "ттн"
    },
    {
        "transaction_name": "",
        "name": "Акт",
        "transaction_code": "",
        "edo_code": "",
        "document_code": "",
        "edo_name": "",
        "document_name": "акт"
    },
    {
        "transaction_name": "",
        "name": "АктСверки",
        "transaction_code": "",
        "edo_code": "",
        "document_code": "",
        "edo_name": "",
        "document_name": "актСверки"
    },
    {
        "transaction_name": "",
        "name": "ПлатПоруч",
        "transaction_code": "",
        "edo_code": "",
        "document_code": "",
        "edo_name": "",
        "document_name": "платПоруч"
    },
    {
        "transaction_name": "",
        "name": "Договор",
        "transaction_code": "",
        "edo_code": "",
        "document_code": "",
        "edo_name": "",
        "document_name": "договор"
    },
    {
        "transaction_name": "",
        "name": "Заказ",
        "transaction_code": "",
        "edo_code": "",
        "document_code": "",
        "edo_name": "",
        "document_name": "заказ"
    },
    {
        "transaction_name": "",
        "name": "СтруктурированныеДанные",
        "transaction_code": "",
        "edo_code": "",
        "document_code": "",
        "edo_name": "",
        "document_name": "структурированныеДанные"
    },
]


class EDIGate(object):

    '''
    Формат описания шлюза:

    {
        КодДокументооборота : {
            КодТранзакции : {
                {
                    КодТипаДокумента: тип док-та по РОСЭУ
                }
            }
        }
    }

    '''

    protocol_map = {
        '20': {  # Счет-фактура
            '01': {
                '01': "СФ",
            },
            '06': {
                '02': "ИзвещениеОПолучении",
            },
            '09': {
                '07': "УведомлениеОбУточнении",
            },
            '10': {
                '06': "ИзвещениеОПолучении",
            },
        },
        '21': {  # Акт
            '01': {
                '01': "АктТитулИсполнителя",
            },
            '03': {
                '02':  "ИзвещениеОПолучении",
            },
            '04': {
                '03': "АктТитулЗаказчика",
                '04': "УведомлениеОбУточнении",
            },
        },
        '22': {  # Торг 12
            '01': {
                '01': "Торг12ТитулПродавца",
            },
            '03': {
                '02':  "ИзвещениеОПолучении",
            },
            '04': {
                '03': "Торг12ТитулПокупателя",
                '04': "УведомлениеОбУточнении",
            },
        },
        '23': {  # Неформализованный ДО
            '01': {
                '01': "Неформализованный",
            },
            '02': {
                '01': "Неформализованный",
            },
            '03': {
                '02':  "ИзвещениеОПолучении",
            },
            '04': {
                '04': u'УведомлениеОбУточнении',
            },
        }
    }

    def __init__(self):
        # Обратные ссылки для удобства поиска
        for edo_code, edo in self.protocol_map.items():
            for trans_code, doctypes in sorted(edo.items()):
                if trans_code == '01':
                    roseu_starting_doc = doctypes['01']
                    # Для определения по типу документа РОСЭУ типа
                    # документооборота EDI
                    self.protocol_map[roseu_starting_doc] = edo_code
                # Также в рамках транзакции ставим обратные ссылки от типов
                # документов РОСЭУ на коды типов документов EDI
                for doccode, roseu_doctype in doctypes.items():
                    doctypes[roseu_doctype] = doccode

    def get_transaction_code(self, edi_doc_code, edi_last_transaction, roseu_doc_type):
        transactions = sorted(self.protocol_map[edi_doc_code].iteritems())
        for trans in transactions:
            if trans[0] < edi_last_transaction:
                continue
            if roseu_doc_type in trans[1].values():
                return trans[0]

    def __getitem__(self, key):
        return self.protocol_map.get(key)
