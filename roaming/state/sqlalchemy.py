#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import absolute_import, division, unicode_literals, print_function

from sqlalchemy import Column, ForeignKey, Integer, String
from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declared_attr
from uuid import uuid4


class SequenceMixin(object):

    '''
    Модель отдельного документооборота. Имеет UID и код типа документооборота.
    В документооборот входит несколько
    контейнеров (Package), каждый из которых представляет отдельную транзакцию.

    '''
    @declared_attr
    def __tablename__(cls):
        return 'sequences'

    id = Column(Integer, primary_key=True)
    uid = Column(String, index=True)
    edi_code = Column(String)

    @declared_attr
    def packages(cls):
        return relationship("Package", backref="sequence")


class PackageMixin(object):

    '''
    Модель контейнера. Контейнер имеет имя файла, код типа транзакции и
    привязан к документообороту (Sequence). В контейнер входят несколько
    документов (Document).

    '''
    @declared_attr
    def __tablename__(cls):
        return 'packages'

    id = Column(Integer, primary_key=True)
    filename = Column(String, index=True)
    trans_code = Column(String)

    @declared_attr
    def documents(cls):
        return relationship("Document", backref="package")

    @declared_attr
    def sequence_id(cls):
        return Column(Integer, ForeignKey("sequences.id"))

    @declared_attr
    def message_id(cls):
        return Column(Integer, ForeignKey("messages.id"))


class DocumentMixin(object):

    '''
    Модель документа. Документ имеет UID и код типа документа.

    '''
    @declared_attr
    def __tablename__(cls):
        return 'documents'

    id = Column(Integer, primary_key=True)
    uid = Column(String, index=True)
    doc_code = Column(String)

    @declared_attr
    def package_id(cls):
        return Column(Integer, ForeignKey("packages.id"))


class MessageMixin(object):

    '''
    Модель логического сообщения РОСЭУ. Имеет UID. Одно сообщение может содержать
    несколько разных контейнеров, которые входят в разные последовательности
    соответственно.

    '''
    @declared_attr
    def __tablename__(cls):
        return 'messages'

    id = Column(Integer, primary_key=True)
    uid = Column(String, index=True)

    @declared_attr
    def packages(cls):
        return relationship("Package", backref="message")


class SQLAlchemyState(object):

    '''
    Абстрагирует хранение состояния в БД с доступом через sqlalchemy.
    Экземпляру передаются конструктор сессии sqlalchemy и модели
    для хранения данных о документообороте, контейнере, ЛС и документе.
    Модели должны наследовать от миксинов, описанных выше.


    '''

    def __init__(self, session, seq_model, package_model, message_model, document_model):
        self.session = session
        self.Sequence = seq_model
        self.Package = package_model
        self.Message = message_model
        self.Document = document_model

    def get_sequence(self, uid):
        return self.session.query(self.Sequence).filter_by(uid=uid).first()

    def add_sequence(self, edi_code, uid=None):
        res = self.Sequence(uid=uid or uuid4().hex, edi_code=edi_code)
        self.session.add(res)
        self.session.commit()
        return res

    def lookup_sequence(self, doc):
        return doc.package.sequence

    def add_message(self, uid):
        msg = self.session.query(self.Message).filter_by(uid=uid).first()
        # assert not msg, 'Message {0} already exists'.format(uid)
        if not msg:
            msg = self.Message(uid=uid)
        self.session.add(msg)
        self.session.commit()
        return msg

    def get_package(self, filename):
        res = self.session.query(self.Package).filter_by(filename=filename).first()
        if not res:
            res = self.Package(filename=filename)
            self.session.add(res)
            self.session.commit()
        return res

    def update_package(self, package, trans_code):
        (self.session.query(self.Package)
         .filter_by(filename=package.filename)
         .update({'trans_code': trans_code}))

    def add_package(self, sequence, message, package):
        sequence.packages.append(package)
        message.packages.append(package)
        self.session.add_all([package, sequence, message])
        self.session.commit()

    def add_document(self, package, uid, doc_code):
        print(package.documents)
        doc = self.session.query(self.Document).filter_by(uid=uid).first()
        # assert not doc, 'Document {0} already exists'.format(uid)
        if not doc:
            doc = self.Document(uid=uid, doc_code=doc_code)
        package.documents.append(doc)
        self.session.add(doc)
        self.session.commit()
        return doc

    def get_main_doc(self, series):
        # XXX: Предполагается, что все серии начинаются с документа с кодом 01 и
        # типом транзакции 01
        q = self.session.query(self.Document).join(self.Package)
        doc = q.filter(self.Package.sequence_id == series.id,
                       self.Package.trans_code == '01',
                       self.Document.doc_code == '01').first()
        return doc.uid if doc else None

    def get_document(self, uid):
        doc = self.session.query(self.Document).filter_by(uid=uid).first()
        return doc

    def get_message(self, uid):
        msg = self.session.query(self.Message).filter_by(uid=uid).first()
        return msg
