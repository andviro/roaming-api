#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import absolute_import, division, unicode_literals, print_function

from mongoengine import *  # noqa
from uuid import uuid4


class SequenceBase(Document):

    '''
    Модель отдельного документооборота. Имеет UID и код типа документооборота.
    В документооборот входит несколько
    контейнеров (Package), каждый из которых представляет отдельную транзакцию.

    '''
    meta = {
        'allow_inheritance': True,
    }

    uid = StringField(required=True, unique=True)
    edi_code = StringField()
    packages = ListField(ReferenceField('PackageBase'))


class PackageBase(Document):

    '''
    Модель контейнера. Контейнер имеет имя файла, код типа транзакции и
    привязан к документообороту (Sequence). В контейнер входят несколько
    документов (Document).

    '''
    meta = {
        'allow_inheritance': True,
    }

    filename = StringField(required=True, unique=True)
    trans_code = StringField()
    documents = ListField(ReferenceField('DocumentBase'))
    sent = DateTimeField()
    received = DateTimeField()
    error = StringField()


class DocumentBase(Document):

    '''
    Модель документа. Документ имеет UID и код типа документа.

    '''
    meta = {
        'allow_inheritance': True,
    }

    uid = StringField(required=True, unique=True)
    doc_code = StringField()


class MessageBase(Document):

    '''
    Модель логического сообщения РОСЭУ. Имеет UID. Одно сообщение может содержать
    несколько разных контейнеров, которые входят в разные последовательности
    соответственно.

    '''
    meta = {
        'allow_inheritance': True,
    }

    uid = StringField(required=True, unique=True)
    packages = ListField(ReferenceField('PackageBase'))


class MongoState(object):

    '''
    Абстрагирует хранение состояния в БД с доступом через mongoengine.
    Экземпляру передаются модели для хранения данных о документообороте,
    контейнере, ЛС и документе.
    Модели должны наследовать от базовых документов, описанных выше.


    '''

    def __init__(self, session, seq_model, package_model, message_model, document_model):
        self.Sequence = seq_model
        self.Package = package_model
        self.Message = message_model
        self.Document = document_model

    def get_sequence(self, uid):
        return self.Sequence.objects(uid=uid).first()

    def add_sequence(self, edi_code, uid=None):
        return self.Sequence(edi_code=edi_code, uid=uid or uuid4().hex).save()

    def lookup_sequence(self, doc):
        pkg = self.Package.objects.get(documents__in=[doc])
        return self.Sequence.objects.get(packages__in=[pkg])

    def add_message(self, uid):
        msg, created = self.Message.objects.get_or_create(uid=uid)
        return msg

    def get_package(self, filename):
        res, _ = self.Package.objects.get_or_create(filename=filename)
        return res

    def update_package(self, package, trans_code):
        self.Package.objects(id=package.id).update_one(set__trans_code=trans_code)

    def add_package(self, sequence, message, package):
        self.Sequence.objects(id=sequence.id).update_one(push__packages=package)
        self.Message.objects(id=message.id).update_one(push__packages=package)

    def add_document(self, package, uid, doc_code):
        res, created = self.Document.objects.get_or_create(uid=uid, doc_code=doc_code)
        self.Package.objects(id=package.id).update_one(push__documents=res)
        return res

    def get_main_doc(self, sequence):
        # XXX: Предполагается, что все серии начинаются с документа с кодом 01 и
        # типом транзакции 01
        docs = sequence.packages[0].documents if len(sequence.packages) else []
        for doc in docs:
            if doc.doc_code == '01':
                return doc.uid
        return None

    def get_document(self, uid):
        return self.Document.objects(uid=uid).first()

    def get_message(self, uid):
        return self.Message.objects(uid=uid).first()
