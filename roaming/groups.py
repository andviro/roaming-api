#!/usr/bin/env python
# coding: utf-8


class Groups(object):
    """Собирает документы в сообщении по связности"""

    class Doc(object):
        """Элемент дерева документов"""

        def __init__(self, docid, docref):
            """Ячейка-документ, хранит в себе список дочерних элементов

            :docid: идентификатор документа
            :docref: ссылка "КДокументу"

            """
            self.id = docid
            self.ref = docref
            self.lst = []

        def __repr__(self):
            return "Doc({self.id}, {self.ref}, {self.lst!s})".format(self=self)

    def __init__(self):
        """ """
        self.elements = {}
        self.refs = {}

    def add(self, docid, docref):
        """Добавление в дерево

        :docid: идентификатор документа
        :docref: ссылка "КДокументу"
        :returns: объект Doc

        Поддерживает два словаря:
            - items, в котором хранятся глобальные группы документов, ключом
            является поле КДокументу
            - refs, для быстрого доступа к дочерним ветвям дерева
        Если добавляемый документ имеет ссылку, которая уже есть в refs, его
        можно подцепить к соотв. подгруппе. Если его ИД есть в items, он
        становится родительским элементом для группы из items и заменяет их
        собой.

        """
        assert docid not in self.refs, \
            u'документ {0} уже есть группах'.format(docid)
        assert docid != docref, \
            u'документ {0} ссылается сам на себя'.format(docid)
        assert docref not in self.refs or docid not in self.elements, \
            u'Документ {0} образует кольцо'.format(docid)

        newdoc = self.Doc(docid, docref)
        self.refs[docid] = newdoc.lst

        # подцепляем элемент к одному из листьев дерева
        if docref in self.refs:
            self.refs[docref].append(newdoc)
        else:
            self.elements[docref] = self.elements.get(docref, [])
            self.elements[docref].append(newdoc)

        if docid in self.elements:
            # элемент заменяет собой группу из элементов верхнего уровня
            newdoc.lst.extend(self.elements[docid])
            del self.elements[docid]
        return newdoc

    @staticmethod
    def _flatten(lst):
        res = []
        for doc in lst:
            res.append(doc.id)
            res.extend(Groups._flatten(doc.lst))
        return res

    def __iter__(self):
        """Перебирает группы в виде: (КДокументу, [(ДокИД, КДокументу), ...]),
        сформированные на основе зависимостей между документами. При этом
        каждый документ с пустым полем КДокументу образует отдельную группу.
        Главный документ в группе всегда первый, длина группы всегда >= 1."""
        for key in self.elements:
            if not key:
                for elt in self.elements[key]:
                    yield (key, self._flatten([elt]))
            else:
                yield (key, self._flatten(self[key]))

    def __getitem__(self, key):
        return self.elements[key]


def main():
    docs = [("1", "9"), ("2", ""), ("3", "2"), ("4", "2"), ("5", "1"),
            ("6", "1"), ("7", "1"), ("8", "5"), ("10", "")]
    g = Groups()
    for d in docs:
        g.add(*d)
    for k in g:
        print k


if __name__ == "__main__":
    main()
