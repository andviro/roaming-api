#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import absolute_import, division, unicode_literals, print_function
from M2Crypto import BIO, SMIME, X509, m2


def extract_container(data):
    '''
    Извлечение и проверка подписи CMS-сообщения. Бросает исключение, если
    подпись не бьется с сертификатом из сообщения.

    * `data` -  CMS-сообщение в виде байтовой строки

    Возвращает пару вида: (данные, отпечаток сертификата)

    '''
    inp = BIO.MemoryBuffer(data)
    p7 = SMIME.PKCS7(m2.pkcs7_read_bio_der(inp._ptr()), 1)
    certStack = p7.get0_signers(X509.X509_Stack())
    thumb = certStack[0].get_fingerprint('sha1')
    st = X509.X509_Store()
    mime = SMIME.SMIME()
    mime.set_x509_store(st)
    mime.set_x509_stack(certStack)
    result = mime.verify(p7, flags=SMIME.PKCS7_NOVERIFY)
    return result, thumb


def sign_container(data, key, cert):
    '''
    Подписывание данных сертификатом и упаковка в сообщение CMS

    * `data` - подписываемые данные в виде байтовой строки
    * `key` - закрытый ключ для подписи, закодированный в PEM
    * `cert` - сертификат для подписи, закодированный в PEM

    Возвращает CMS-сообщение, закодированное в DER, в виде байтовой строки

    '''
    buf = BIO.MemoryBuffer(data)
    mime = SMIME.SMIME()
    key_bio = BIO.File(open(key))
    cert_bio = BIO.File(open(cert))
    mime.load_key_bio(key_bio, cert_bio)
    pkcs7 = mime.sign(buf, flags=SMIME.PKCS7_BINARY)
    out = BIO.MemoryBuffer()
    pkcs7.write_der(out)
    return out.read()
