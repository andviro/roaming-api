#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import division, unicode_literals, print_function
from roaming import (TransportPackage as BaseTransportPackage,
                     LogicalMessage as BaseLogicalMessage, Receipt, ContainerEDI)
from roaming.rsa import extract_container, sign_container
from roaming.state.sqlalchemy import (SQLAlchemyState, SequenceMixin,
                                      PackageMixin, DocumentMixin, MessageMixin)
from zipfile import ZipFile
from io import BytesIO

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base

engine = create_engine('sqlite:///:memory:')
Base = declarative_base(bind=engine)
Session = sessionmaker(bind=engine)


class Sequence(Base, SequenceMixin):
    pass


class Package(Base, PackageMixin):
    pass


class Message(Base, MessageMixin):
    pass


class Document(Base, DocumentMixin):
    pass

Base.metadata.create_all()

test_state = SQLAlchemyState(Session(), Sequence, Package, Message, Document)


class TransportPackage(BaseTransportPackage):
    state = test_state


class LogicalMessage(BaseLogicalMessage):
    state = test_state


def test_load_tp():
    """ Загрузка ТП из данных или файла """
    data = 'testcases/logical.cms'
    tp1 = TransportPackage.load(data)
    assert not tp1.error, tp1.error

    bindata = open(data, 'rb').read()
    tp2 = TransportPackage.load(bindata, 'logical.cms')
    assert not tp2.error, tp2.error

    filedata = open(data, 'rb')
    tp3 = TransportPackage.load(filedata)
    assert not tp3.error, tp3.error

    tp4 = TransportPackage.load(b'alskdjaslkdjaskldjlaskjdaslkd', 'test.cms')
    assert tp4.error, tp4.error

    tp5 = TransportPackage.load('testcases/wrong_names.cms')
    assert tp5.error, tp5.error

    tp6 = TransportPackage.load('testcases/created.cms')
    assert not tp6.error, tp6.error


def test_unpack_receipts():
    """ Перечисление квитанций в ТП """
    data = 'testcases/receipt.cms'
    tp1 = TransportPackage.load(data)
    assert not tp1.error, tp1.error
    for thing in tp1.unpack():
        assert isinstance(thing, Receipt)
        assert thing.receipt_type in ('error', 'success', 'unknown_id')


def test_unpack_messages():
    """ Перечисление ЛС в ТП """
    data = 'testcases/logical.cms'
    tp1 = TransportPackage.load(data)
    assert not tp1.error, tp1.error
    for thing in tp1.unpack():
        assert isinstance(thing, BaseLogicalMessage)


def test_message_contents():
    """ Перечисление и загрузка файлов в ЛС"""
    data = 'testcases/logical.cms'
    tp1 = TransportPackage.load(data)
    msg = list(tp1.unpack())[0]
    msglist = list(msg.namelist())
    assert all(msglist) and len(msglist) == 3
    for fn in msglist:
        assert len(msg.read(fn))


def test_crypto():
    """ Подписывание и распаковка данных в CMS-сообщении """
    msg = b'asldkjsdlkj alskdjlaskjdlsaj dalskd jlasdkjlaskdj'
    signed_data = sign_container(msg, 'testcases/key.pem', 'testcases/cert.pem')
    extracted_data, thumbprint = extract_container(signed_data)
    assert thumbprint == 'E4FCD442957FF7037C58A569A0853D93D3B65CEC'
    assert msg == extracted_data


def test_save_zip():
    """ Создание ТП копированием существующего и сохранение в ZIP """
    for fn in ('logical.cms', 'receipt.cms'):
        tp1 = TransportPackage.load('testcases/{0}'.format(fn))
        tp2 = TransportPackage()
        for msg in tp1.unpack():
            tp2.add(msg)
        zf = ZipFile(BytesIO(tp2.save_zip()))
        assert zf.namelist()


def test_save_cms():
    """ Создание ТП копированием существующего и сохранение в CMS """
    for fn in ('logical.cms', 'receipt.cms'):
        tp1 = TransportPackage.load('testcases/{0}'.format(fn))
        tp2 = TransportPackage()
        for msg in tp1.unpack():
            tp2.add(msg)
        cms = tp2.save_cms('testcases/key.pem', 'testcases/cert.pem')
        tp3 = TransportPackage.load(cms, 'test.cms')
        assert not tp3.error, tp3.error


def test_load_edi():
    """ Загрузка объекта ContainerEDI из файла """
    fn = 'testcases/EDI_2AED2CA87F3-236E-4C63-9836-2B357CA86132_2AEB2103C81-CEAD-4B31-966D-8425F924D0DA_2122ecaef87b429db114650edff9af06_20_01_01.zip'
    edi1 = ContainerEDI.load(fn)
    assert len(edi1.namelist())
    return edi1


def test_message_from_edi():
    """ Преобразование контейнера EDI в ЛС """
    edi1 = test_load_edi()
    msg = LogicalMessage.from_edi(edi1)
    assert msg.uid
    assert not msg.error, msg.error
    return msg


def test_tp_from_edi():
    """ Преобразование контейнера EDI в ТП """
    msg = test_message_from_edi()
    tp = TransportPackage()
    tp.add(msg)
    cms = tp.save_cms('testcases/key.pem', 'testcases/cert.pem')
    tp2 = TransportPackage.load(cms, 'test.cms')
    assert not tp2.error, tp2.error


def test_edi_from_tp():
    """ Распаковка ТП и преобразование ЛС в EDI """
    data = 'testcases/logical.cms'
    tp = TransportPackage.load(data)
    tp2 = TransportPackage()
    for msg in tp.unpack():
        test_state.add_message(msg.uid)
        edis = msg.convert_to_edi()
        assert not msg.error, msg.error
        assert len(edis)
        assert all(edi.raw_content for edi in edis)
        open('out_edi.zip', 'wb').write(edis[0].raw_content)
        ls = LogicalMessage.from_edi(edis[0])
        tp2.add(ls)
    assert not tp2.error, tp2.error
    tp2.save_zip('out_tp.zip')


def test_send():
    """ Отправка ТП """
    return True
    data = 'testcases/logical.cms'
    tp = TransportPackage.load(data)
    status, reason, data = tp.send('https://diadoc-roaming.kontur.ru/',
                                   'https://report.keydisk.ru/edo/',
                                   'privateAE.pem', 'publicAE.pem')
    tp.save_zip('logical.zip')
    print(status, reason, data)
    assert status == 200


if __name__ == '__main__':
    pass
